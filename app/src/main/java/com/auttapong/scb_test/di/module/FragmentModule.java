package com.auttapong.scb_test.di.module;

import com.auttapong.scb_test.ui.main.favorite.FavoriteListFragment;
import com.auttapong.scb_test.ui.main.mobile.MobileListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract MobileListFragment contributeMobileListFragment();

    @ContributesAndroidInjector
    abstract FavoriteListFragment contributeFavoriteListFragment();
}
