package com.auttapong.scb_test.di.module;

import android.app.Application;

import com.auttapong.scb_test.SCBTestApplication;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class AppModule {
    @Binds
    abstract Application provideContext(SCBTestApplication application);
}