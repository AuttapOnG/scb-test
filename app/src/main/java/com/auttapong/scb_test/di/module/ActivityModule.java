package com.auttapong.scb_test.di.module;

import com.auttapong.scb_test.ui.detail.MobileDetailActivity;
import com.auttapong.scb_test.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = {FragmentModule.class})
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract MobileDetailActivity contributeMobileDetailActivity();
}
