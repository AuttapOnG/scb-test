package com.auttapong.scb_test.di;

import com.auttapong.scb_test.SCBTestApplication;
import com.auttapong.scb_test.di.module.ActivityModule;
import com.auttapong.scb_test.di.module.AppModule;
import com.auttapong.scb_test.di.module.FragmentModule;
import com.auttapong.scb_test.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(
        modules = {
                AppModule.class,
                ActivityModule.class,
                FragmentModule.class,
                NetworkModule.class
        }
)
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(SCBTestApplication application);

        AppComponent build();
    }

    void inject(SCBTestApplication application);
}