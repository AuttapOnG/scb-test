package com.auttapong.scb_test.service;

import com.auttapong.scb_test.model.MobileResponse;
import com.auttapong.scb_test.model.MobileImageResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceAPI {
    @GET("api/mobiles/")
    Observable<List<MobileResponse>> getMobileList();

    @GET("api/mobiles/{mobileId}/images/")
    Observable<List<MobileImageResponse>> getMobileImageList(@Path("mobileId") int mobileId);
}
