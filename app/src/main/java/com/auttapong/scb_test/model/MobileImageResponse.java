package com.auttapong.scb_test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileImageResponse {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("mobile_id")
    @Expose
    private int mobileId;

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public int getMobileId() {
        return mobileId;
    }
}
