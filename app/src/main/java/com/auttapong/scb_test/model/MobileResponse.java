package com.auttapong.scb_test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileResponse {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("thumbImageURL")
    @Expose
    private String thumbImageURL;
    @SerializedName("description")
    @Expose
    private String description;

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    public Double getPrice() {
        return price;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public String getDescription() {
        return description;
    }
}
