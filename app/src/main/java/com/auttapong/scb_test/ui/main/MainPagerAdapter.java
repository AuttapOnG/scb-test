package com.auttapong.scb_test.ui.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class MainPagerAdapter extends FragmentStatePagerAdapter {
    private List<MainTab> mainTabList;

    public MainPagerAdapter(FragmentActivity view, List<MainTab> mainTabList) {
        super(view.getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mainTabList = mainTabList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mainTabList.get(position).fragment;
    }

    @Override
    public int getCount() {
        return mainTabList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mainTabList.get(position).title;
    }

    public static class MainTab {
        private Fragment fragment;
        private String title;

        public MainTab setFragment(Fragment fragment) {
            this.fragment = fragment;
            return this;
        }

        public MainTab setTitle(String title) {
            this.title = title;
            return this;
        }
    }
}
