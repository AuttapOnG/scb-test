package com.auttapong.scb_test.ui.base.exception;

public class ViewNotAttachedException extends RuntimeException {
    public ViewNotAttachedException() {
        super("Please call Presenter.attachView(MvpBaseView) before requesting data to the View");
    }
}