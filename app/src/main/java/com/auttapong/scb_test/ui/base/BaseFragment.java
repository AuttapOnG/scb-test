package com.auttapong.scb_test.ui.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.auttapong.scb_test.di.Injectable;
import com.auttapong.scb_test.ui.base.exception.NotSetLayoutException;
import com.auttapong.scb_test.ui.base.exception.PresenterNotCreateException;

public abstract class BaseFragment<P extends BaseInterface.Presenter> extends Fragment implements BaseInterface.View, Injectable {
    private P presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutResId = getLayoutView();
        setRetainInstance(true);
        if (getLayoutView() == 0) throw new NotSetLayoutException();
        return inflater.inflate(layoutResId, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = createPresenter();
        presenter.attachView(this);
        bindView(view);
        setupInstance();
        setupView();
        getPresenter().onViewCreate();
        if (savedInstanceState == null) {
            initialize();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onViewStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onViewStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getPresenter().onViewDestroy();
        presenter.detachView();
    }


    @Override
    public P getPresenter() {
        if (presenter != null) return presenter;
        throw new PresenterNotCreateException();
    }

    public abstract P createPresenter();

    public abstract int getLayoutView();

    public abstract void bindView(View view);

    public abstract void setupInstance();

    public abstract void setupView();

    public abstract void initialize();
}
