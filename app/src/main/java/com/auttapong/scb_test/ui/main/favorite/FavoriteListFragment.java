package com.auttapong.scb_test.ui.main.favorite;

import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.base.BaseFragment;
import com.auttapong.scb_test.ui.detail.MobileDetailActivity;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;

import java.util.List;

public class FavoriteListFragment
        extends BaseFragment<FavoriteListFragmentInterface.Presenter>
        implements FavoriteListFragmentInterface.View, FavoriteListAdapter.OnItemClickListener {
    public static FavoriteListFragment newInstance() {
        FavoriteListFragment fragment = new FavoriteListFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private FavoriteListAdapter adapter;

    @Override
    public FavoriteListFragmentInterface.Presenter createPresenter() {
        return new FavoriteListFragmentPresenter();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_favorite_list;
    }

    @Override
    public void bindView(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    @Override
    public void setupInstance() {

    }

    @Override
    public void setupView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new FavoriteListAdapter();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void setListAdapter(List<Mobile> mobileList) {
        adapter.setData(mobileList);
        adapter.notifyDataSetChanged();
    }

    public void sortBy(SortBy sortBy) {
        getPresenter().sortBy(sortBy);
    }

    @Override
    public void onItemClickListener(Mobile mobile) {
        Intent intent = new Intent(getActivity(), MobileDetailActivity.class);
        intent.putExtra(MobileDetailActivity.MOBILE, mobile);
        getActivity().startActivity(intent);
    }
}
