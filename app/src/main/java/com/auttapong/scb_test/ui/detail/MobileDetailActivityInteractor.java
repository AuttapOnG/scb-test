package com.auttapong.scb_test.ui.detail;

import com.auttapong.scb_test.model.MobileImageResponse;
import com.auttapong.scb_test.service.ServiceAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;

public class MobileDetailActivityInteractor implements MobileDetailActivityInterface.Interactor {
    private ServiceAPI serviceAPI;

    @Inject
    public MobileDetailActivityInteractor(ServiceAPI serviceAPI) {
        this.serviceAPI = serviceAPI;
    }

    @Override
    public ReplaySubject<List<String>> getMobileDetail(int id) {
        final ReplaySubject<List<String>> imageList = ReplaySubject.create();
        serviceAPI.getMobileImageList(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MobileImageResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<MobileImageResponse> mobileImageResponses) {
                        List<String> images = new ArrayList<>();
                        for (MobileImageResponse mobileImageResponse : mobileImageResponses) {
                            images.add(mobileImageResponse.getUrl());
                        }
                        imageList.onNext(images);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return imageList;
    }
}
