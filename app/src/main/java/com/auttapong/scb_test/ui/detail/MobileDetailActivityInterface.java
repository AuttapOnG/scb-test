package com.auttapong.scb_test.ui.detail;

import com.auttapong.scb_test.ui.base.BaseInterface;

import java.util.List;

import io.reactivex.subjects.ReplaySubject;

public class MobileDetailActivityInterface {
    interface View extends BaseInterface.View {

        void setImageAdapter(List<String> imageList);
    }

    interface Presenter extends BaseInterface.Presenter<MobileDetailActivityInterface.View> {

        void getMobileDetail(int id);
    }

    interface Interactor {
        ReplaySubject<List<String>> getMobileDetail(int id);
    }
}
