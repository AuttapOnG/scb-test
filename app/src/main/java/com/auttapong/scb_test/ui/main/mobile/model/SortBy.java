package com.auttapong.scb_test.ui.main.mobile.model;

public enum SortBy {
    LowToHigh,
    HighToLow,
    Rating
}
