package com.auttapong.scb_test.ui.main;

import android.app.Dialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.base.BaseActivity;
import com.auttapong.scb_test.ui.main.favorite.FavoriteListFragment;
import com.auttapong.scb_test.ui.main.mobile.MobileListFragment;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity
        extends BaseActivity<MainActivityInterface.Presenter>
        implements View.OnClickListener {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MainPagerAdapter pagerAdapter;
    private Dialog dialogSortBy;
    private RadioButton rbLowToHigh, rbHighToLow, rbRating;
    private List<MainPagerAdapter.MainTab> mainTabList;
    private MobileListFragment mobileListFragment;
    private FavoriteListFragment favoriteListFragment;

    @Override
    public MainActivityInterface.Presenter createPresenter() {
        return new MainActivityPresenter();
    }

    @Override
    public int getLayoutView() {
        return R.layout.activity_main;
    }

    @Override
    public void bindView() {
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        dialogSortBy = new Dialog(this);
        dialogSortBy.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSortBy.setContentView(R.layout.dialog_sort);
        dialogSortBy.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        rbLowToHigh = dialogSortBy.findViewById(R.id.rb_low_to_high);
        rbHighToLow = dialogSortBy.findViewById(R.id.rb_high_to_low);
        rbRating = dialogSortBy.findViewById(R.id.rb_rating);
        rbHighToLow.setOnClickListener(this);
        rbLowToHigh.setOnClickListener(this);
        rbRating.setOnClickListener(this);
    }

    @Override
    public void setupInstance() {
        mobileListFragment = MobileListFragment.newInstance();
        favoriteListFragment = FavoriteListFragment.newInstance();
        mainTabList = new ArrayList<>();
        mainTabList.add(new MainPagerAdapter.MainTab()
                .setFragment(mobileListFragment)
                .setTitle(getString(R.string.tab_mobile)));
        mainTabList.add(new MainPagerAdapter.MainTab()
                .setFragment(favoriteListFragment)
                .setTitle(getString(R.string.tab_favorite)));
        pagerAdapter = new MainPagerAdapter(this, mainTabList);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void setupView() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void initialize() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_sort) {
            dialogSortBy.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (rbLowToHigh == view) {
            mobileListFragment.sortBy(SortBy.LowToHigh);
            favoriteListFragment.sortBy(SortBy.LowToHigh);
            dialogSortBy.dismiss();
        } else if (rbHighToLow == view) {
            mobileListFragment.sortBy(SortBy.HighToLow);
            favoriteListFragment.sortBy(SortBy.HighToLow);
            dialogSortBy.dismiss();
        } else if (rbRating == view) {
            mobileListFragment.sortBy(SortBy.Rating);
            favoriteListFragment.sortBy(SortBy.Rating);
            dialogSortBy.dismiss();
        }
    }
}
