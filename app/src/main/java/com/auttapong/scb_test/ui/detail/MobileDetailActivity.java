package com.auttapong.scb_test.ui.detail;

import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.base.BaseActivity;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;

import java.util.List;

import javax.inject.Inject;

public class MobileDetailActivity
        extends BaseActivity<MobileDetailActivityInterface.Presenter>
        implements MobileDetailActivityInterface.View {
    public static final String MOBILE = "MOBILE_INTENT";
    @Inject
    MobileDetailActivityPresenter presenter;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TextView tvPrice, tvRating, tvDescription;
    private MobileImagePagerAdapter mobileImagePagerAdapter;

    @Override
    public MobileDetailActivityInterface.Presenter createPresenter() {
        return presenter;
    }

    @Override
    public int getLayoutView() {
        return R.layout.activity_mobile_detail;
    }

    @Override
    public void bindView() {
        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.view_pager);
        tvPrice = findViewById(R.id.tv_price);
        tvRating = findViewById(R.id.tv_rating);
        tvDescription = findViewById(R.id.tv_description);
    }

    @Override
    public void setupInstance() {

    }

    @Override
    public void setupView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mobileImagePagerAdapter = new MobileImagePagerAdapter(this);
        viewPager.setAdapter(mobileImagePagerAdapter);
    }

    @Override
    public void initialize() {
        Mobile mobile = getIntent().getParcelableExtra(MOBILE);
        getPresenter().getMobileDetail(mobile.getId());
        tvPrice.setText(getString(R.string.card_price) + mobile.getPrice());
        tvRating.setText(getString(R.string.card_rating) + mobile.getRating());
        tvDescription.setText(mobile.getDescription());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setImageAdapter(List<String> imageList) {
        mobileImagePagerAdapter.setUpData(imageList);
        mobileImagePagerAdapter.notifyDataSetChanged();
    }
}
