package com.auttapong.scb_test.ui.main.mobile;

import com.auttapong.scb_test.model.MobileResponse;
import com.auttapong.scb_test.service.ServiceAPI;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;

public class MobileListFragmentInteractor implements MobileListFragmentInterface.Interactor {
    private ServiceAPI serviceAPI;

    @Inject
    public MobileListFragmentInteractor(ServiceAPI serviceAPI) {
        this.serviceAPI = serviceAPI;
    }

    @Override
    public ReplaySubject<List<Mobile>> getMobileDataList() {
        final ReplaySubject<List<Mobile>> mobileListObservable = ReplaySubject.create();
        serviceAPI.getMobileList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MobileResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<MobileResponse> value) {
                        final List<Mobile> mobileList = new ArrayList<>();
                        for (MobileResponse mobileResponse : value) {
                            mobileList.add(new Mobile(mobileResponse.getId(),
                                    mobileResponse.getName(),
                                    mobileResponse.getRating(),
                                    mobileResponse.getPrice(),
                                    mobileResponse.getThumbImageURL(),
                                    mobileResponse.getDescription()));
                        }
                        mobileListObservable.onNext(mobileList);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return mobileListObservable;
    }
}
