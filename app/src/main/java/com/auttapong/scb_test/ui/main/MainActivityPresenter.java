package com.auttapong.scb_test.ui.main;

import com.auttapong.scb_test.ui.base.BasePresenter;

public class MainActivityPresenter extends BasePresenter<MainActivityInterface.View> implements MainActivityInterface.Presenter {
    @Override
    public void onViewCreate() {

    }

    @Override
    public void onViewDestroy() {

    }

    @Override
    public void onViewStart() {

    }

    @Override
    public void onViewStop() {

    }
}
