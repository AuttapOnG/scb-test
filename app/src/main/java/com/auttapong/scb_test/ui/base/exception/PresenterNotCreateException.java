package com.auttapong.scb_test.ui.base.exception;

public class PresenterNotCreateException extends RuntimeException {
    public PresenterNotCreateException() {
        super("Please call createPresenter() before requesting data to the Presenter");
    }
}