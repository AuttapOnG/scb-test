package com.auttapong.scb_test.ui.detail;

import com.auttapong.scb_test.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;

public class MobileDetailActivityPresenter
        extends BasePresenter<MobileDetailActivityInterface.View>
        implements MobileDetailActivityInterface.Presenter {

    private MobileDetailActivityInteractor interactor;

    @Inject
    public MobileDetailActivityPresenter(MobileDetailActivityInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onViewCreate() {

    }

    @Override
    public void onViewDestroy() {

    }

    @Override
    public void onViewStart() {

    }

    @Override
    public void onViewStop() {

    }

    @Override
    public void getMobileDetail(int id) {
        interactor.getMobileDetail(id).subscribe(new Consumer<List<String>>() {
            @Override
            public void accept(List<String> strings) throws Exception {
                getView().setImageAdapter(strings);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {

            }
        });
    }
}
