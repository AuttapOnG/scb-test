package com.auttapong.scb_test.ui.main.favorite;

import com.auttapong.scb_test.ui.base.BaseInterface;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;

import java.util.List;

public class FavoriteListFragmentInterface {
    interface View extends BaseInterface.View {

        void setListAdapter(List<Mobile> mobileList);
    }

    public interface Presenter extends BaseInterface.Presenter<FavoriteListFragmentInterface.View> {

        void sortBy(SortBy sortBy);
    }
}
