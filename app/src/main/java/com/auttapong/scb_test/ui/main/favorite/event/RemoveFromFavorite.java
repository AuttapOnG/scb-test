package com.auttapong.scb_test.ui.main.favorite.event;

import com.auttapong.scb_test.ui.main.mobile.model.Mobile;

public class RemoveFromFavorite {
    private Mobile mobile;

    public RemoveFromFavorite(Mobile mobile) {
        this.mobile = mobile;
    }

    public Mobile getMobile() {
        return mobile;
    }
}
