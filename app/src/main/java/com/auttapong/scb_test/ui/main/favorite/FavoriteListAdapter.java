package com.auttapong.scb_test.ui.main.favorite;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class FavoriteListAdapter extends RecyclerView.Adapter<FavoriteListAdapter.ViewHolder> {
    private List<Mobile> mobiles = new ArrayList<>();
    private OnItemClickListener listener;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setUpView(mobiles.get(position));
    }

    @Override
    public int getItemCount() {
        return mobiles.size();
    }

    public void setData(List<Mobile> mobileList) {
        this.mobiles = mobileList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private ImageView ivThumb;
        private TextView tvName, tvPrice, tvRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            ivThumb = itemView.findViewById(R.id.iv_thumb);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvRating = itemView.findViewById(R.id.tv_rating);
        }

        public void setUpView(final Mobile mobile) {
            Glide.with(ivThumb).load(mobile.getThumbImageURL()).into(ivThumb);
            tvName.setText(mobile.getName());
            tvPrice.setText(tvPrice.getContext().getString(R.string.card_price) + mobile.getPrice());
            tvRating.setText(tvRating.getContext().getString(R.string.card_rating) + mobile.getRating());
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClickListener(mobile);
                }
            });
        }
    }

    interface OnItemClickListener{
        void onItemClickListener(Mobile mobile);
    }
}
