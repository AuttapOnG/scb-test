package com.auttapong.scb_test.ui.main.favorite;

import android.util.Log;

import com.auttapong.scb_test.ui.base.BasePresenter;
import com.auttapong.scb_test.ui.main.favorite.event.AddToFavorite;
import com.auttapong.scb_test.ui.main.favorite.event.RemoveFromFavorite;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FavoriteListFragmentPresenter
        extends BasePresenter<FavoriteListFragmentInterface.View>
        implements FavoriteListFragmentInterface.Presenter {
    private List<Mobile> mobileList = new ArrayList<>();
    private SortBy sortBy = SortBy.LowToHigh;

    @Override
    public void onViewCreate() {
        RxBus.get().register(this);
    }

    @Override
    public void onViewDestroy() {
        RxBus.get().unregister(this);
    }

    @Override
    public void onViewStart() {

    }

    @Override
    public void onViewStop() {

    }

    @Subscribe
    public void addToFavorite(AddToFavorite addToFavorite) {
        mobileList.add(addToFavorite.getMobile());
        sortBy(sortBy);
    }

    @Subscribe
    public void RemoveFromFavorite(RemoveFromFavorite removeFromFavorite) {
        mobileList.remove(removeFromFavorite.getMobile());
        sortBy(sortBy);
    }

    @Override
    public void sortBy(final SortBy sortBy) {
        this.sortBy = sortBy;
        Collections.sort(mobileList, new Comparator<Mobile>() {
            @Override
            public int compare(Mobile mobile, Mobile t1) {
                if (sortBy == SortBy.LowToHigh) {
                    return mobile.getPrice().compareTo(t1.getPrice());
                } else if (sortBy == SortBy.HighToLow) {
                    return t1.getPrice().compareTo(mobile.getPrice());
                } else {
                    return t1.getRating().compareTo(mobile.getRating());
                }
            }
        });
        getView().setListAdapter(mobileList);
    }
}
