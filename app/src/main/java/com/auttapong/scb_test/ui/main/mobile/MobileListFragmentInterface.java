package com.auttapong.scb_test.ui.main.mobile;

import com.auttapong.scb_test.ui.base.BaseInterface;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;

import java.util.List;

import io.reactivex.subjects.ReplaySubject;

public class MobileListFragmentInterface {
    interface View extends BaseInterface.View {

        void setListAdapter(List<Mobile> mobiles);
    }

    public interface Presenter extends BaseInterface.Presenter<MobileListFragmentInterface.View> {
        void getMobileList();

        void sortBy(SortBy sortBy);

        void addToFavorite(Mobile mobile);

        void removeFromFavorite(Mobile mobile);
    }

    interface Interactor {
        ReplaySubject<List<Mobile>> getMobileDataList();
    }
}
