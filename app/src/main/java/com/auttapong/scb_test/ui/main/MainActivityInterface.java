package com.auttapong.scb_test.ui.main;

import com.auttapong.scb_test.ui.base.BaseInterface;

public class MainActivityInterface {
    interface View extends BaseInterface.View {

    }

    interface Presenter extends BaseInterface.Presenter<MainActivityInterface.View> {

    }
}
