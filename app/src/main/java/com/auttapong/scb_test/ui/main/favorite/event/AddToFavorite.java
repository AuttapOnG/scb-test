package com.auttapong.scb_test.ui.main.favorite.event;

import com.auttapong.scb_test.ui.main.mobile.model.Mobile;

public class AddToFavorite {
    private Mobile mobile;

    public AddToFavorite(Mobile mobile) {
        this.mobile = mobile;
    }

    public Mobile getMobile() {
        return mobile;
    }
}
