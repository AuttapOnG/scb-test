package com.auttapong.scb_test.ui.base.exception;

public class NotSetLayoutException extends RuntimeException {
    public NotSetLayoutException(){
        super( "getLayoutView() must be return 0" );
    }
}