package com.auttapong.scb_test.ui.base;

import com.auttapong.scb_test.ui.base.exception.ViewNotAttachedException;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V extends BaseInterface.View> implements BaseInterface.Presenter<V> {
    private WeakReference<V> view;

    @Override
    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public V getView() throws NullPointerException {
        if (view != null) return view.get();
        throw new ViewNotAttachedException();
    }
}
