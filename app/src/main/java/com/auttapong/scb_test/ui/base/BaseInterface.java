package com.auttapong.scb_test.ui.base;

public abstract class BaseInterface {
    public interface View {
        Presenter getPresenter();
    }

    public interface Presenter<V extends BaseInterface.View> {
        void attachView(V view);

        void detachView();

        V getView();

        void onViewCreate();

        void onViewDestroy();

        void onViewStart();

        void onViewStop();
    }
}
