package com.auttapong.scb_test.ui.main.mobile;

import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.base.BaseFragment;
import com.auttapong.scb_test.ui.detail.MobileDetailActivity;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;

import java.util.List;

import javax.inject.Inject;

public class MobileListFragment
        extends BaseFragment<MobileListFragmentInterface.Presenter>
        implements MobileListFragmentInterface.View, MobileListAdapter.OnItemClickListener {
    public static MobileListFragment newInstance() {
        MobileListFragment fragment = new MobileListFragment();
        return fragment;
    }

    @Inject
    MobileListFragmentPresenter presenter;

    private RecyclerView recyclerView;
    private MobileListAdapter adapter;

    @Override
    public MobileListFragmentInterface.Presenter createPresenter() {
        return presenter;
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_mobile_list;
    }

    @Override
    public void bindView(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    @Override
    public void setupInstance() {

    }

    @Override
    public void setupView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MobileListAdapter();
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void initialize() {
        getPresenter().getMobileList();
    }

    @Override
    public void setListAdapter(List<Mobile> mobiles) {
        adapter.setData(mobiles);
        adapter.notifyDataSetChanged();
    }

    public void sortBy(SortBy sortBy) {
        getPresenter().sortBy(sortBy);
    }

    @Override
    public void addToFavorite(Mobile mobile) {
        getPresenter().addToFavorite(mobile);
    }

    @Override
    public void removeFromFavorite(Mobile mobile) {
        getPresenter().removeFromFavorite(mobile);
    }

    @Override
    public void itemClick(Mobile mobile) {
        Intent intent = new Intent(getActivity(), MobileDetailActivity.class);
        intent.putExtra(MobileDetailActivity.MOBILE, mobile);
        getActivity().startActivity(intent);
    }
}
