package com.auttapong.scb_test.ui.main.mobile;

import com.auttapong.scb_test.ui.base.BasePresenter;
import com.auttapong.scb_test.ui.main.favorite.event.AddToFavorite;
import com.auttapong.scb_test.ui.main.favorite.event.RemoveFromFavorite;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.auttapong.scb_test.ui.main.mobile.model.SortBy;
import com.hwangjr.rxbus.RxBus;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;

public class MobileListFragmentPresenter
        extends BasePresenter<MobileListFragmentInterface.View>
        implements MobileListFragmentInterface.Presenter {

    private MobileListFragmentInteractor interactor;
    private List<Mobile> mobileList;
    private SortBy sortBy = SortBy.LowToHigh;

    @Inject
    public MobileListFragmentPresenter(MobileListFragmentInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onViewCreate() {
        RxBus.get().register(this);
    }

    @Override
    public void onViewDestroy() {
        RxBus.get().unregister(this);
    }

    @Override
    public void onViewStart() {

    }

    @Override
    public void onViewStop() {

    }

    @Override
    public void getMobileList() {
        interactor.getMobileDataList().subscribe(new Consumer<List<Mobile>>() {
            @Override
            public void accept(List<Mobile> mobiles) throws Exception {
                mobileList = mobiles;
                sortBy(sortBy);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
            }
        });
    }

    @Override
    public void sortBy(final SortBy sortBy) {
        this.sortBy = sortBy;
        Collections.sort(mobileList, new Comparator<Mobile>() {
            @Override
            public int compare(Mobile mobile, Mobile t1) {
                if (sortBy == SortBy.LowToHigh) {
                    return mobile.getPrice().compareTo(t1.getPrice());
                } else if (sortBy == SortBy.HighToLow) {
                    return t1.getPrice().compareTo(mobile.getPrice());
                } else {
                    return t1.getRating().compareTo(mobile.getRating());
                }
            }
        });
        getView().setListAdapter(mobileList);
    }

    @Override
    public void addToFavorite(Mobile mobile) {
        RxBus.get().post(new AddToFavorite(mobile));
    }

    @Override
    public void removeFromFavorite(Mobile mobile) {
        RxBus.get().post(new RemoveFromFavorite(mobile));
    }
}
