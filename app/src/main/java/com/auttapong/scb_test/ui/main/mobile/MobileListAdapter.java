package com.auttapong.scb_test.ui.main.mobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.auttapong.scb_test.R;
import com.auttapong.scb_test.ui.main.mobile.model.Mobile;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class MobileListAdapter extends RecyclerView.Adapter<MobileListAdapter.ViewHolder> {
    private List<Mobile> mobiles = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mobile_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setUpView(mobiles.get(position));
    }

    @Override
    public int getItemCount() {
        return mobiles.size();
    }

    public void setData(List<Mobile> mobiles) {
        this.mobiles = mobiles;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private ImageView ivThumb, ivFavorite;
        private TextView tvName, tvDescription, tvPrice, tvRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            ivThumb = itemView.findViewById(R.id.iv_thumb);
            ivFavorite = itemView.findViewById(R.id.iv_favorite);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvRating = itemView.findViewById(R.id.tv_rating);
        }

        public void setUpView(final Mobile mobile) {
            Glide.with(ivThumb).load(mobile.getThumbImageURL()).into(ivThumb);
            tvName.setText(mobile.getName());
            tvDescription.setText(mobile.getDescription());
            tvPrice.setText(tvPrice.getContext().getString(R.string.card_price) + mobile.getPrice());
            tvRating.setText(tvRating.getContext().getString(R.string.card_rating) + mobile.getRating());
            if (mobile.isFavorite()) {
                ivFavorite.setImageDrawable(ContextCompat.getDrawable(ivFavorite.getContext(), R.drawable.ic_fill_heart));
            } else {
                ivFavorite.setImageDrawable(ContextCompat.getDrawable(ivFavorite.getContext(), R.drawable.ic_heart));
            }
            ivFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mobile.isFavorite()) {
                        mobile.setFavorite(false);
                        onItemClickListener.removeFromFavorite(mobile);
                        ivFavorite.setImageDrawable(ContextCompat.getDrawable(ivFavorite.getContext(), R.drawable.ic_heart));
                    } else {
                        mobile.setFavorite(true);
                        onItemClickListener.addToFavorite(mobile);
                        ivFavorite.setImageDrawable(ContextCompat.getDrawable(ivFavorite.getContext(), R.drawable.ic_fill_heart));
                    }
                }
            });
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.itemClick(mobile);
                }
            });
        }
    }

    interface OnItemClickListener {
        void addToFavorite(Mobile mobile);

        void removeFromFavorite(Mobile mobile);

        void itemClick(Mobile mobile);
    }
}
