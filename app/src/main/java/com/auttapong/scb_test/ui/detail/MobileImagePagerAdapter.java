package com.auttapong.scb_test.ui.detail;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MobileImagePagerAdapter extends FragmentStatePagerAdapter {
    List<String> imageList;

    public MobileImagePagerAdapter(FragmentActivity view) {
        super(view.getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.imageList = new ArrayList<>();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        ImageFragment fragment = new ImageFragment(imageList.get(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void setUpData(List<String> imageList) {
        this.imageList = imageList;
    }
}
