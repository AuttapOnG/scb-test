package com.auttapong.scb_test.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.auttapong.scb_test.R;
import com.bumptech.glide.Glide;

public class ImageFragment extends Fragment {
    private String imageUrl;
    private ImageView imageView;

    public ImageFragment(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        imageView = view.findViewById(R.id.image_view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!imageUrl.startsWith("http")) {
            imageUrl = "http://" + imageUrl;
        }
        Glide.with(getActivity()).load(imageUrl).into(imageView);
    }
}
