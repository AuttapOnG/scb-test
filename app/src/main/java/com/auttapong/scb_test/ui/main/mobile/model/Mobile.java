package com.auttapong.scb_test.ui.main.mobile.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Mobile implements Parcelable {
    private int id;
    private String name;
    private Double rating;
    private Double price;
    private String thumbImageURL;
    private String description;
    private boolean isFavorite;

    public Mobile(int id, String name, Double rating, Double price, String thumbImageURL, String description) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.price = price;
        this.thumbImageURL = thumbImageURL;
        this.description = description;
    }

    protected Mobile(Parcel in) {
        id = in.readInt();
        name = in.readString();
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readDouble();
        }
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
        thumbImageURL = in.readString();
        description = in.readString();
        isFavorite = in.readByte() != 0;
    }

    public static final Creator<Mobile> CREATOR = new Creator<Mobile>() {
        @Override
        public Mobile createFromParcel(Parcel in) {
            return new Mobile(in);
        }

        @Override
        public Mobile[] newArray(int size) {
            return new Mobile[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    public Double getPrice() {
        return price;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        if (rating == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(rating);
        }
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(price);
        }
        parcel.writeString(thumbImageURL);
        parcel.writeString(description);
        parcel.writeByte((byte) (isFavorite ? 1 : 0));
    }
}
