package com.auttapong.scb_test.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.auttapong.scb_test.ui.base.exception.NotSetLayoutException;
import com.auttapong.scb_test.ui.base.exception.PresenterNotCreateException;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseActivity<P extends BaseInterface.Presenter> extends AppCompatActivity implements BaseInterface.View, HasSupportFragmentInjector {
    private P presenter;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        getPresenter().attachView(this);
        int layoutId = getLayoutView();
        if (layoutId == 0) throw new NotSetLayoutException();
        setContentView(layoutId);
        bindView();
        setupInstance();
        setupView();
        getPresenter().onViewCreate();
        if (savedInstanceState == null) {
            initialize();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPresenter().onViewStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPresenter().onViewStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onViewDestroy();
        presenter.detachView();
    }

    @Override
    public P getPresenter() {
        if (presenter != null) return presenter;
        throw new PresenterNotCreateException();
    }

    public abstract P createPresenter();

    public abstract int getLayoutView();

    public abstract void bindView();

    public abstract void setupInstance();

    public abstract void setupView();

    public abstract void initialize();
}
